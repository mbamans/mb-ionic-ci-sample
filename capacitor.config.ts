import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'mb-ionic-ci-sample',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
